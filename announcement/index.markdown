---
layout: nosidebar
---

<div class="text-center well">
  <h1 class="text-danger">YOU GOT THE WEDDING ANNOUCEMENT</h1>
</div>
<div class="row well well-xs">
  <div class="col-xs-5 text-center">
    <h2 class="text-danger">HERO</h2>
    <img src="{{ "/images/link.png" | prepend: site.baseurl }}" class="img-responsive center-block">
    <h3 class="text-danger">Thomas Albert Webster III</h3>
  </div>
  <div class="col-xs-2">
    <h1 class="text-danger text-center-block">AND</h1>
  </div>
  <div class="col-xs-5 text-center">
    <h2 class="text-danger">PRINCESS</h2>
    <img src="{{ "/images/zelda.png" | prepend: site.baseurl }}" class="img-responsive center-block">
    <h3 class="text-danger">Renee Lynne Faller</h3>
  </div>
</div>
<div class="row well well-xs">
  <div class="col-xs-12">
    <h3 class="text-center">Were wed on</h3>
    <h3 class="text-center">Saturday, October 11th, 2014</h3>
    <h3 class="text-center">in Centerville, Ohio</h3>
  </div>
</div>
<div class="row well well-xs">
  <div class="row">
    <div class="col-xs-12">
      <img src="{{ "/images/oldman.png" | prepend: site.baseurl }}" class="img-responsive center-block">
      <br>
      <br>
    </div>
  </div>
  <div class="col-xs-4">
    <img src="{{ "/images/link_up.png" | prepend: site.baseurl }}" class="img-responsive center-block">
  </div>
  <div class="col-xs-4">
    <img src="{{ "/images/rings.png" | prepend: site.baseurl }}" class="img-responsive center-block">
  </div>
  <div class="col-xs-4">
    <img src="{{ "/images/zelda_up.png" | prepend: site.baseurl }}" class="img-responsive center-block">
  </div>
</div>
<div class="row well well-xs">
  <div class="col-xs-12">
    <h3 class="text-center">The forces of <span class="text-danger">evil</span> prevented us from having many of you there</h3>
    <h3 class="text-center">But you were fondly in our <span class="text-danger">heart containers</span></h3>
  </div>
</div>
<div class="row well well-xs">
  <div class="col-xs-12">
    <img src="{{ "/images/triforce.png" | prepend: site.baseurl }}" class="img-responsive center-block">
    <h3 class="text-center">May the way of the hero lead to the triforce</h3>
  </div>
</div>
<div class="row well well-xs">
  <div class="col-xs-12">
    <a href="{{ "/pictographs/" | prepend: site.baseurl }}">
      <h3 class="text-center text-primary">Pictographs Available Here</h3>
    </a>
  </div>
</div>
