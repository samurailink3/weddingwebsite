/* TEAM */
Your title: Tom "SamuraiLink3" Webster
Site: samurailink3.com
Google Plus: +TomWebster
Twitter: @samurailink3
Location: Dayton, Ohio

/* THANKS */
Name: Nintendo (www.nintendo.com)
Name: Renee Faller (Now known as Renee Webster)

/* SITE */
Standards: HTML5, CSS3, JS
Components: Jekyll, jQuery, Twitter Bootstrap, Google Analytics
Software: Sublime Text 3, GIMP, Mozilla Firefox
