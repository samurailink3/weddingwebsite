---
layout: default
---

## The Sacred Words

<div class="well">
Ladies, Gentlemen, Children of all ages. Today we gather here today in the Hyrulean matrimony known as marriage.
<br>
<br>
Renee do you take Tom to be the man you spend the rest of your life with, despite him not being able to give directions
until the exact moment that you absolutely need them? Despite him not being able to grow a full beard until the ripe old age of 35? Do you take him to be your Ocarina of Time to his Nintendo 64, your Peach to his Mario?
<br>
<br>
Tom do you take Renee to be the woman you spend the rest of your life with? To be the Navi to your Link, the Medic to your Heavy Weapons guy, the Roll to your Rock? The Princess what's her name to your Earthworm Jim?
<br>
<br>
If anyone wishes to speak against this marital acceptance....you probably should have done something before today...it's kinda already done.
<br>
<br>
By the power vested in me by The Golden Goddesses in partnership with Talos. I now pronounce you Husband and Bride. You now may kiss the prince and princess!
</div>
