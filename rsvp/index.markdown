---
layout: default
navbar: RSVP
---

## RSVP

<p class="text-warning">Note: No plus ones. We're keeping this small.</p>

RSVP by sending an email to: [thewebsters@weddingoftime.com](mailto:thewebsters@weddingoftime.com)

## Dress Code

Be casual. Really casual. WHOA! NOT THAT CASUAL! Please wear some type of clothes, anything. I don't care if you ensconce yourself in velvet or take up the hero's clothes. We'll be wearing jeans and t-shirts.