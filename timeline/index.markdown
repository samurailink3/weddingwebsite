---
layout: default
navbar: Timeline
---

<h3 class="center">The Wedding takes place on <span style="color: red;">October 11th, 2014</span></h3>

<div class="well">
  <table class="table table-striped table-bordered table-responsive table-hover">
    <tr>
      <th>Time</th>
      <th>Event</th>
      <th>Place</th>
    </tr>
    <tr>
      <td>3:00PM - 3:30PM</td>
      <td>Ceremony</td>
      <td><a href="{{ "/map/#house" | prepend: site.baseurl }}">House</td>
    </tr>
    <tr>
      <td>5:00PM - 7:00PM</td>
      <td>Dinner</td>
      <td><a href="{{ "/map/#house" | prepend: site.baseurl }}">House</td>
    </tr>
    <tr>
      <td>7:30PM - ??????</td>
      <td>After Party</td>
      <td><a href="{{ "/map/#scene75" | prepend: site.baseurl }}">Scene75</td>
    </tr>
  </table>
</div>