---
layout: default
navbar: Home
---

### Once upon a time, in a galaxy far far away...

# ZOMBIES
<img class="img-responsive pull-left" style="margin: 0px 15px 0px 0px" src="{{ "/images/zombie1.png" | prepend: site.baseurl }}">

What most people don't realize about Tom and Renee is that they are actually very careful planners. From their wedding planning, to figuring out exactly how much lumber would be required to fortify their apartment building against hordes of the walking dead, they always have a plan.

One day, while researching and comparing Z-Day defense structures online, Tom suddenly found himself in a conversation with the only other person to out-do him in zombie preparations: Renee Faller.

One word:

# Bandits
<img class="img-responsive pull-left" style="margin: 0px 15px 0px 0px" src="{{ "/images/knight1.png" | prepend: site.baseurl }}">

Tom was awestruck, his plan included everything… from potable water to renewable sources of electricity and small scale agriculture, but HUMANS! They were the real enemy! Tom realized that effective zombie apocalypse planning and survival was not possible without Renee on his team. So he set out on a mission…

Renee knew Tom Webster was an ideal choice for Team Survival, but when asked to see him face to face, she had doubts as to whether this interview would confirm that choice. Little did she know that interview would last 27 hours. The interview tested many of the skills they had both trained for, offensive driving ranges, low-gravity obstacle courses, firearm training, and unlimited french fry eating contests. By the end, Renee was exhausted, but it was time to meet the rest of Tom's team.

# The Brett Test.
<img class="img-responsive pull-left" style="margin: 0px 15px 0px 0px" src="{{ "/images/wizard1.png" | prepend: site.baseurl }}">

Only one of Tom’s team had the clairvoyance to test whether or not Renee was a suitable addition. Brett. Little did anyone suspect, Renee would pass with flying colors. 

Fast forward one year, and Renee is sitting in a local eatery unknowing about the upcoming ambush. Tom reached into his pocket while her attention was diverted. He presented a small black box. Inside, a small metallic circle with a small chunk of corundum. Renee said yes.

Zombies brought these two together and they will eventually tear them apart.

Because brains.

<a id="bunny" class="btn center-block" rel="popover" title="<span class='h3'>BUNNY!</span>" data-img="/images/bunny.jpg">
</a>